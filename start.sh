#!/usr/bin/env bash

if [ -f ./.env ]; then
    set -a # automatically export all variables
    source .env
    set +a
fi

base="mkcert -cert-file certs/local.pem -key-file certs/local-key.pem $(< domains.txt)"

export DC_BIN=${DC_BIN:-"docker-compose"}

$DC_BIN down

echo $base
$base

$DC_BIN up -d --build
